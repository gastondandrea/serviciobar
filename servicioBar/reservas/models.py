from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=30, blank=False)
    email = models.EmailField(max_length=50, unique=True, blank=False)
    password = models.CharField(max_length=128, blank=False)
    phone = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Reservation(models.Model):
    date = models.DateField(blank=False)
    time = models.TimeField(blank=False)
    number_of_people = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(20)])
    confirmed = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return f'Reserva el {self.date} a las {self.time}'
    



class ReservationReminder(models.Model):
    send_date = models.DateTimeField()
    reservation = models.ForeignKey(Reservation, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return f'Recordatorio enviado el {self.send_date}'